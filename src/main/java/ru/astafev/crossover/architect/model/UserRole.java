package ru.astafev.crossover.architect.model;

public enum UserRole {
    PUBLISHER,
    PUBLIC_USER
}
