package ru.astafev.crossover.architect.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Entity(name = "journals")
@Data
@EqualsAndHashCode(exclude = {"subscriptions"})
@ToString(exclude = {"subscriptions"})
public class Journal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(min = 1, max = 255)
    @NotBlank
    private String name;

    @Column(length = 2048)
    private String description;

    @OneToMany(mappedBy = "journal", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private List<Subscription> subscriptions;

    @ManyToMany
    @JoinTable(
            name = "journals_themes",
            joinColumns = @JoinColumn(name = "journal_id", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "theme_id", referencedColumnName = "ID"))
    private Set<Theme> themes;

}
