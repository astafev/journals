package ru.astafev.crossover.architect.model;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Entity(name = "users")
public class User {

    @Id
    private String username;

    @Size(min = 1, max = 50)
    String password;

    private String email;

    private Boolean enabled = true;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Authority> authorities;
}
