package ru.astafev.crossover.architect.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity(name = "numbers")
@EqualsAndHashCode(exclude = "file")
@ToString(exclude = "file")
public class Number {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(min = 1, max = 255)
    private String name;

    @Column(length = 2048)
    private String description;

    @ManyToOne
    @JoinColumn(name = "journal_id")
    private Journal journal;

    @ManyToOne
    @JoinColumn(name = "publisher_id")
    private User publisher;

    private Date published;

    @Column(name = "file_size")
    private Integer fileSize;

    /**
     * 20 megabytes is maximum size for the pdf
     * TODO consider creating a property in application.yml with check on frontend
     * */
    @Column(columnDefinition = "BLOB(20M)")
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @JsonIgnore
    @NotNull
    private byte[] file;

}
