package ru.astafev.crossover.architect.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity(name="authorities")
@EqualsAndHashCode(exclude = "user")
@ToString(exclude = "user")
public class Authority {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "username")
    private User user;

    @Enumerated(EnumType.STRING)
    private UserRole authority;

}