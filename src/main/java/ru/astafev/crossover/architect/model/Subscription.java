package ru.astafev.crossover.architect.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "subscriptions")
public class Subscription {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /** so far we don't need mapping with real User here*/
    private String user;

    @ManyToOne
    @JoinColumn(name = "journal_id")
    private Journal journal;
}
