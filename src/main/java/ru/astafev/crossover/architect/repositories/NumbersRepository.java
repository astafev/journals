package ru.astafev.crossover.architect.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.astafev.crossover.architect.model.Number;

import java.util.List;

public interface NumbersRepository extends PagingAndSortingRepository<Number, Long> {

    List<Number> findAllByJournalIdOrderByPublishedDesc(Long journalId);

    Number findOneByJournalIdAndId(Long journalId, Long id);

    void deleteByJournalIdAndId(Long journalId, Long id);

    @Query("select n.file from ru.astafev.crossover.architect.model.Number n where n.id = ?2 and n.journal.id = ?1")
    byte[] getFile(Long journalId, Long id);
}
