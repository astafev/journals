package ru.astafev.crossover.architect.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.astafev.crossover.architect.model.Journal;

public interface JournalsRepository extends PagingAndSortingRepository<Journal, Long> {

}
