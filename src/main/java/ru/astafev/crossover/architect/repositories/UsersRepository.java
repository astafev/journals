package ru.astafev.crossover.architect.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.astafev.crossover.architect.model.User;

public interface UsersRepository extends CrudRepository<User, String> {

}
