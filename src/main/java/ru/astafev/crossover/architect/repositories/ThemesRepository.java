package ru.astafev.crossover.architect.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.astafev.crossover.architect.model.Theme;

import java.util.Optional;

public interface ThemesRepository extends CrudRepository<Theme, Long> {

    Optional<Theme> findByName(String name);

}
