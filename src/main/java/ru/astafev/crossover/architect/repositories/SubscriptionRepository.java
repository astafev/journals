package ru.astafev.crossover.architect.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.astafev.crossover.architect.model.Journal;
import ru.astafev.crossover.architect.model.Subscription;

import java.util.List;
import java.util.Optional;

public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {

    List<Subscription> findAllByUser(String user);

    List<Subscription> findAllByJournal(Journal journal);

    Optional<Subscription> findByUserAndJournal(String user, Journal journal);

}
