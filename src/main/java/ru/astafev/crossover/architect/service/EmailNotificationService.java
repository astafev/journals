package ru.astafev.crossover.architect.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.astafev.crossover.architect.model.Number;
import ru.astafev.crossover.architect.model.User;
import ru.astafev.crossover.architect.repositories.UsersRepository;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
@ConditionalOnProperty("notification.email")
@Slf4j
public class EmailNotificationService implements NotificationService.NewNumberNotification {
    public static final String NOTIFICATION_MESSAGE = "New number in \"{0}\" journal: {1}";

    @Autowired
    MailSender mailSender;

    @Value("spring.mail.username")
    String mailNotificationFrom;

    @Autowired
    UsersRepository usersRepository;

    @Override
    @Transactional
    public void sendNotification(Number number) {
        List<User> subscribers = retrieveSubscribers(number);
        subscribers.parallelStream().forEach(user -> {
            if (!StringUtils.isEmpty(user.getEmail())) {
                final SimpleMailMessage simpleMessage = new SimpleMailMessage();
                simpleMessage.setSubject(
                        MessageFormat.format(NOTIFICATION_MESSAGE, number.getJournal().getName(), number.getName()));
                simpleMessage.setText("Visit our site to read the new number.");
                simpleMessage.setTo(user.getEmail());
                simpleMessage.setFrom(mailNotificationFrom);
                log.debug("sending new number notification to " + user.getEmail());
                mailSender.send(simpleMessage);
            }
        });
    }


    List<User> retrieveSubscribers(Number number) {
        return number.getJournal().getSubscriptions().stream()
                .map(subscription -> usersRepository.findOne(subscription.getUser()))
                .collect(Collectors.toList());
    }
}
