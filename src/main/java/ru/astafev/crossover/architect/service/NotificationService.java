package ru.astafev.crossover.architect.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.astafev.crossover.architect.model.User;
import ru.astafev.crossover.architect.model.Number;
import ru.astafev.crossover.architect.repositories.UsersRepository;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NotificationService {

    /**
     * send notifications in another thread
     * */
    private static final Executor executor = Executors.newSingleThreadExecutor();

    @Autowired(required = false)
    List<NewNumberNotification> notificationSenders;


    public void notifyAboutNewNumber(Number number) {
        executor.execute(() -> notify(number));
    }

    void notify(Number number) {
        if (notificationSenders != null) {
            notificationSenders.stream().forEach(service -> {
                try {
                    service.sendNotification(number);
                } catch (Exception e) {
                    log.error("error on notification about new number. notifier: " + service.toString(), e);
                }
            });
        }
    }


    @FunctionalInterface
    public interface NewNumberNotification {
        void sendNotification(Number number);
    }
}
