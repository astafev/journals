package ru.astafev.crossover.architect.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.security.RolesAllowed;

@Controller
public class ViewController {

    @RequestMapping(path = "login")
    public String loginPage() {
        return "login";
    }

    @RequestMapping(path = "/app/numbers-list/numbers-list.view.html")
    public String angularTemplates() {
        return "app/numbers-list/numbers-list.view";
    }

    @RolesAllowed("ROLE_PUBLIC_USER")
    @RequestMapping(path = {"/"})
    public String indexPage() {
        return "index";
    }
}
