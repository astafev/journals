package ru.astafev.crossover.architect.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import ru.astafev.crossover.architect.model.Journal;
import ru.astafev.crossover.architect.model.Subscription;
import ru.astafev.crossover.architect.model.Theme;
import ru.astafev.crossover.architect.repositories.JournalsRepository;
import ru.astafev.crossover.architect.repositories.SubscriptionRepository;
import ru.astafev.crossover.architect.repositories.ThemesRepository;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = JournalController.PATH)
public class JournalController {

    static final String PATH = "/api/journals";

    @Autowired
    JournalsRepository journalsRepository;
    @Autowired
    SubscriptionRepository subscriptionRepository;
    @Autowired
    ThemesRepository themesRepository;

    @RequestMapping(method = RequestMethod.POST)
    @RolesAllowed("ROLE_PUBLISHER")
    @Transactional
    public ResponseEntity saveJournal(@Valid @RequestBody Journal journal) throws URISyntaxException {
        if (journal.getThemes() != null) {
            Set<Theme> dbThemes = journal.getThemes().stream().filter(theme -> !StringUtils.isEmpty(theme.getName()))
                    .map(theme -> {
                        Optional<Theme> dbTheme = themesRepository.findByName(theme.getName());
                        if (dbTheme.isPresent()) {
                            return dbTheme.get();
                        } else {
                            return themesRepository.save(theme);
                        }
                    }).collect(Collectors.toSet());
            journal.setThemes(dbThemes);
        }
        Long id = journalsRepository.save(journal).getId();
        return ResponseEntity
                .created(new URI(String.format("%s/%s", JournalController.PATH, String.valueOf(id))))
                .build();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public ResponseEntity getJournal(@PathVariable("id") Long id) {
        Journal one = journalsRepository.findOne(id);
        if (one == null) {
            return ResponseEntity.status(404).build();
        } else {
            return ResponseEntity.ok(one);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    @RolesAllowed("ROLE_PUBLISHER")
    public ResponseEntity deleteJournal(@PathVariable("id") Long id) {
        Journal one = journalsRepository.findOne(id);
        if (one == null) {
            return ResponseEntity.status(404).build();
        } else if(!CollectionUtils.isEmpty(one.getSubscriptions())) {
            return ResponseEntity.status(403).build();
        } else {
            journalsRepository.delete(one);
            return ResponseEntity.ok().build();
        }
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{id}/subscribe")
    @RolesAllowed("ROLE_PUBLIC_USER")
    @Transactional
    public ResponseEntity subscribe(@PathVariable("id") Long id)
            throws URISyntaxException {
        final Journal journal = journalsRepository.findOne(id);

        String username = getUsername();
        if (!subscriptionRepository.findByUserAndJournal(username, journal).isPresent()) {
            Subscription subscription = new Subscription();
            subscription.setJournal(journal);
            subscription.setUser(username);
            subscriptionRepository.save(subscription);
        }

        return ResponseEntity.created(new URI(String.format("%s/%d/subscribe", JournalController.PATH, id))).build();
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}/subscribe")
    @RolesAllowed("ROLE_PUBLIC_USER")
    @Transactional
    public ResponseEntity unsubscribe(@PathVariable("id") Long id) {
        Optional<Subscription> subscription = subscriptionRepository.findByUserAndJournal(getUsername(),
                journalsRepository.findOne(id));
        if (subscription.isPresent()) {
            subscriptionRepository.delete(subscription.get());
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getJournals() {
        return ResponseEntity.ok(journalsRepository.findAll());
    }

    @RequestMapping(method = RequestMethod.GET, path = "/subscribed")
    public ResponseEntity getSubscribedJournals() {
        List<Journal> journals = subscriptionRepository.findAllByUser(getUsername()).stream().map(Subscription::getJournal)
                .collect(Collectors.toList());
        return ResponseEntity.ok(journals);
    }

    /**
     * retrieve information about authenticated user from SecurityContext
     */
    String getUsername() {
        return ((org.springframework.security.core.userdetails.User)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
    }


}
