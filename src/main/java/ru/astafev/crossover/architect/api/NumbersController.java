package ru.astafev.crossover.architect.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;
import ru.astafev.crossover.architect.model.Journal;
import ru.astafev.crossover.architect.model.Number;
import ru.astafev.crossover.architect.repositories.JournalsRepository;
import ru.astafev.crossover.architect.repositories.NumbersRepository;
import ru.astafev.crossover.architect.service.NotificationService;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.Date;

@RestController
@RequestMapping(path = JournalController.PATH + "/{journalId}/numbers")
@Slf4j
public class NumbersController {

    @Autowired
    NumbersRepository numberRepository;
    @Autowired
    JournalsRepository journalsRepository;
    @Autowired
    NotificationService notificationService;

    byte[] getFileContent(StandardMultipartHttpServletRequest request) {
        final String type = "data:application/pdf;base64";
        String file = request.getParameter("file");
        if (file == null || !file.startsWith(type)) {
            throw new IllegalArgumentException("unsupported file type");
        } else {
            try {
                return Base64.getDecoder().decode(file.substring(type.length() + 1).getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                // will not happen
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Saves number that we get from frontend as form.
     * <p>
     * Not very good looking piece of code, because can't find a good way to send file from angular, so now it's sent
     * as base64 string.
     */
    @RequestMapping(method = RequestMethod.POST)
    @RolesAllowed("ROLE_PUBLISHER")
    @Transactional
    public ResponseEntity saveNumber(@PathVariable("journalId") Long journalId,
                                     StandardMultipartHttpServletRequest request
    ) throws URISyntaxException {
        final Journal journal = journalsRepository.findOne(journalId);

        Number number = new Number();
        number.setPublished(new Date());
        number.setJournal(journal);
        number.setName(request.getParameter("name"));

        if (StringUtils.isEmpty(number.getName())) {
            throw new IllegalArgumentException("name field is mandatory");
        }

        number.setDescription(request.getParameter("description"));
        number.setFile(getFileContent(request));
        number.setFileSize(number.getFile().length);
        log.debug("Saving new number for journal {}, journal id = {}", journal.getName(), journal.getId());
        Long id = numberRepository.save(number).getId();
        log.debug("Saved new number with id {} for journal {}. Sending notifications.", id, journal.getName());

        notificationService.notifyAboutNewNumber(number);

        return ResponseEntity.created(
                new URI(String.format("%s/%d/numbers/%d", JournalController.PATH, journalId, id))).build();
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{numberId}")
    @RolesAllowed("ROLE_PUBLISHER")
    @Transactional
    public ResponseEntity delete(@PathVariable("journalId") Long journalId,
                                 @PathVariable("numberId") Long numberId) {
        log.debug("deleting number with id {}", numberId);
        numberRepository.deleteByJournalIdAndId(journalId, numberId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getNumbers(@PathVariable("journalId") Long journalId) {
        return ResponseEntity.ok(numberRepository.findAllByJournalIdOrderByPublishedDesc(journalId));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{numberId}")
    @RolesAllowed("ROLE_PUBLIC_USER")
    public ResponseEntity getNumber(@PathVariable("journalId") Long journalId,
                                    @PathVariable("numberId") Long numberId) {
        String username = ((org.springframework.security.core.userdetails.User)
                SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        //TODO not finished
        return ResponseEntity.ok(numberRepository.findOneByJournalIdAndId(journalId,
                numberId));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{numberId}/file")
    public void getFile(@PathVariable("journalId") Long journalId,
                        @PathVariable("numberId") Long numberId,
                        HttpServletResponse response) throws IOException {
        response.setContentType("application/pdf");
        byte[] file = numberRepository.getFile(journalId, numberId);
        StreamUtils.copy(file, response.getOutputStream());
        response.flushBuffer();
    }
}
