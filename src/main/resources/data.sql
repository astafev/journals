INSERT INTO themes (name) VALUES ('Theme 1');
INSERT INTO themes (name) VALUES ('Theme 2');

INSERT INTO journals (name, description) VALUES ('Journal 1',
                                                 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');

INSERT INTO journals_themes (journal_id, theme_id) VALUES (1, 1);
INSERT INTO journals_themes (journal_id, theme_id) VALUES (1, 2);

INSERT INTO journals (name, description) VALUES ('Journal 2',
                                                 'asdasdfasdfasdfasdf sad fasf wqer qwer');
INSERT INTO journals_themes (journal_id, theme_id) VALUES (2, 1);

-- insert numbers with sample small pdf
INSERT INTO numbers (name, description, journal_id, published, file, file_size) VALUES
  ('Number 1',
   'Number description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
   1, '2008-08-08 20:08:08',
   HEXTORAW(
       '255044462d312e300d0a312030206f626a3c3c2f547970652f436174616c6f672f50616765732032203020523e3e656e646f626a20322030206f626a3c3c2f547970652f50616765732f4b6964735b33203020525d2f436f756e7420313e3e656e646f626a20332030206f626a3c3c2f547970652f506167652f4d65646961426f785b302030203320335d3e3e656e646f626a0d0a787265660d0a3020340d0a3030303030303030303020363535333520660d0a30303030303030303130203030303030206e0d0a30303030303030303533203030303030206e0d0a30303030303030313032203030303030206e0d0a747261696c65723c3c2f53697a6520342f526f6f742031203020523e3e0d0a7374617274787265660d0a3134390d0a25454f46'),
   291
  );
INSERT INTO numbers (name, description, journal_id, published, file, file_size) VALUES
  ('Number 2',
   'Number description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
   1, NOW(),
   HEXTORAW(
       '255044462d312e300d0a312030206f626a3c3c2f547970652f436174616c6f672f50616765732032203020523e3e656e646f626a20322030206f626a3c3c2f547970652f50616765732f4b6964735b33203020525d2f436f756e7420313e3e656e646f626a20332030206f626a3c3c2f547970652f506167652f4d65646961426f785b302030203320335d3e3e656e646f626a0d0a787265660d0a3020340d0a3030303030303030303020363535333520660d0a30303030303030303130203030303030206e0d0a30303030303030303533203030303030206e0d0a30303030303030313032203030303030206e0d0a747261696c65723c3c2f53697a6520342f526f6f742031203020523e3e0d0a7374617274787265660d0a3134390d0a25454f46'),
   291
  );


INSERT INTO users (username, password, enabled) VALUES ('user', 'user', TRUE);

INSERT INTO users (username, password, enabled) VALUES ('admin', 'admin', TRUE);
INSERT INTO authorities (username, authority) VALUES ('admin', 'ROLE_PUBLIC_USER');
INSERT INTO authorities (username, authority) VALUES ('admin', 'ROLE_PUBLISHER');
INSERT INTO authorities (username, authority) VALUES ('user', 'ROLE_PUBLIC_USER');

INSERT INTO subscriptions (user, journal_id) VALUES ('user', 1);

UPDATE users SET email = ''
WHERE username = 'user';