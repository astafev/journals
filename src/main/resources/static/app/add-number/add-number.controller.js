'use strict';

angular.module('journalsPortal')
  .controller('AddNumberController', ['$scope', '$routeParams', '$location', 'Numbers', 'Errors',
    function ($scope, $routeParams, $location, Numbers, Errors) {
      $scope.number = {};

      $scope.save = function () {
        Numbers.save({journalId: $routeParams['journalId']}, $scope.number, function () {
          $scope.message = "The number was successfully saved, go back to ";
          $scope.resultLink = "#/journals/" + $routeParams['journalId'];
          $scope.resultPage = "journal page";
        }, function (e) {
          $scope.message = Errors.parseError(e);
          delete $scope.resultLink;
          delete $scope.resultPage;
        });

      }
    }
  ])
  .directive("fileread", [function () {
    return {
      scope: {
        fileread: "="
      },
      link: function (scope, element) {
        element.bind("change", function (changeEvent) {
          var reader = new FileReader();
          reader.onload = function (loadEvent) {
            scope.$apply(function () {
              scope.fileread = loadEvent.target.result;
            });
          };
          reader.readAsDataURL(changeEvent.target.files[0]);
        });
      }
    }
  }]);