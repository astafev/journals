'use strict';

angular.module('journalsPortal')
  .factory('Journals', ['$resource', function ($resource) {
    var journalsApiAddress = "/api/journals/";
    return $resource(
      journalsApiAddress, {}, {
        getOne: {
          method: 'GET', url: journalsApiAddress + '/:id', params: {id: '@id'}
        },
        get: {
          method: 'GET', isArray: true
        },
        getSubscribed: {
          method: 'GET', url: journalsApiAddress + "/subscribed", isArray: true
        },
        subscribe: {
          method: 'POST', url: journalsApiAddress + '/:id/subscribe', params: {id: '@id'}
        },
        unsubscribe: {
          method: 'DELETE', url: journalsApiAddress + '/:id/subscribe', params: {id: '@id'}
        }
      }
    )
  }]);