'use strict';

angular.module('journalsPortal')
  .factory('Errors', [function () {
    return {
      parseError: function (e) {
        var message = e.error;

        if (!message) {
          message = "error occured";
          // spring validation error
          if (e.data && e.data.errors && $.isArray(e.data.errors)) {
            message += ": ";
            angular.forEach(e.data.errors, function (e) {
              message += e.field + ' ' + e.defaultMessage + '; ';
            })
          }
        }

        return message;
      },

      defaultErrorCallback: function (e) {
        console.log(e);
        alert("error occured " + e.message);
      }
    }
  }]);