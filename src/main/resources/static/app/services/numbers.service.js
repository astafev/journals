'use strict';

angular.module('journalsPortal')
  .factory('Numbers', ['$resource', function ($resource) {
    var numbersAddress = "/api/journals/:journalId/numbers";
    
    function formDataObject(data) {
      var fd = new FormData();
      angular.forEach(data, function (value, key) {
        fd.append(key, value);
      });
      return fd;
    }

    return $resource(
      numbersAddress, {journalId: '@journalId'}, {
        getOne: {
          method: 'GET', url: numbersAddress + '/:id', params: {journalId: '@journalId', id: '@numberId'}
        },
        get: {
          method: 'GET', isArray: true
        },
        save: {
          method: 'POST',
          transformRequest: formDataObject,
          headers: {'Content-Type': undefined, enctype: 'multipart/form-data'}
        }
      }
    )
  }])
;