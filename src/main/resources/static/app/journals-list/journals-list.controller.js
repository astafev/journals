'use strict';

angular.module('journalsPortal')
  .controller('JournalsListController', ['$scope', '$location', 'Journals', 'Errors',
    function ($scope, $location, Journals, Errors) {
      $scope.absUrl = '/#/journals';
      $scope.journals = Journals.get({}, function(){}, Errors.defaultErrorCallback);
    }]);