'use strict';

angular.module('journalsPortal').filter('localedate', function () {
  return function (input, locale) {
    //ensure input is a number
    if (isNaN(input) || !angular.isDefined(input)) {
      return input;
    }
    var defaultFormat = {day: "2-digit", month: "2-digit", year: "numeric", hour: "numeric", minute: "2-digit"};
    return new Date(input).toLocaleDateString(locale, defaultFormat);
  };
});