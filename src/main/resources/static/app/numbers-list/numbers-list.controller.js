'use strict';

angular.module('journalsPortal')
  .controller('NumbersListController', ['$scope', '$routeParams', '$location', 'Journals', 'Numbers', 'Errors',
    function ($scope, $routeParams, $location, Journals, Numbers, Errors) {
      var activeJournalId = $routeParams['journalId'];
      $scope.canAddNumber = true;
      $scope.absUrl = '#' + $location.path();
      $scope.journalsUrl = '/#/journals/';
      $scope.subscribedJournals = Journals.getSubscribed({}, function (result) {
        //try to find active journal in subscribed
        angular.forEach(result, function (item) {
            if (item.id == activeJournalId) {
              $scope.activeJournal = item;
              $scope.activeJournal.subscribed = true;
            }
          }
        );
        // if not found make additional request
        if (!$scope.activeJournal) {
          $scope.activeJournal = Journals.getOne({id: activeJournalId}, function () {
          }, Errors.defaultErrorCallback)
        }
      }, Errors.defaultErrorCallback);

      $scope.numbers = Numbers.get({journalId: activeJournalId}, function () {
      }, Errors.defaultErrorCallback);

      $scope.subscribe = function () {
        Journals.subscribe({id: activeJournalId}, function () {
          $scope.activeJournal.subscribed = true;
          $scope.subscribedJournals.push($scope.activeJournal);
        }, Errors.defaultErrorCallback);
      };
      $scope.unsubscribe = function () {
        Journals.unsubscribe({id: activeJournalId}, function () {
          $scope.activeJournal.subscribed = false;
          $scope.subscribedJournals = $.grep($scope.subscribedJournals, function (journal) {
            return journal.id != activeJournalId;
          });
        }, Errors.defaultErrorCallback);
      };
    }
  ]);