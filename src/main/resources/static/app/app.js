'use strict';

var app = angular.module('journalsPortal', [
  'ngRoute',
  'ngResource'
]);

app.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.when('/journals', {
      templateUrl: 'app/journals-list/journals-list.view.html',
      controller: 'JournalsListController'
    }).when('/journals/add', {
      templateUrl: 'app/add-journal/add-journal.view.html',
      controller: 'AddJournalController'
    }).when('/journals/:journalId/edit', {
      templateUrl: 'app/add-journal/add-journal.view.html',
      controller: 'AddJournalController'
    }).when('/journals/:journalId/add', {
      templateUrl: 'app/add-number/add-number.view.html',
      controller: 'AddNumberController'
    }).when('/journals/:journalId', {
      templateUrl: 'app/numbers-list/numbers-list.view.html',
      controller: 'NumbersListController'
    }).otherwise({
      redirectTo: '/journals'
    });
  }]);