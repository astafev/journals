 'use strict';

angular.module('journalsPortal')
  .controller('AddJournalController', ['$scope', 'Journals', 'Errors', '$routeParams',
    function ($scope, Journals, Errors, $routeParams) {
      
      if($routeParams['journalId']) {
        $scope.journal = Journals.getOne({id:$routeParams['journalId']});
      } else {
        $scope.journal = {themes: []};
      }

      $scope.addTheme = function () {
        $scope.journal.themes.push({})
      };

      $scope.deleteTheme = function ($index) {
        $scope.journal.themes.splice($index, 1);
      };
      
      $scope.save = function () {
        Journals.save({}, $scope.journal, function (response, headers) {
          $scope.message = "The journal was successfully saved, visit ";
          $scope.resultLink = "#/journals/" + headers().location.split('/')[3];
          $scope.resultPage = "journal page";
        }, function (e) {
          $scope.message = Errors.parseError(e);
          delete $scope.resultLink;
          delete $scope.resultPage;

        });

      }
    }
  ]);