package ru.astafev.crossover.architect.service;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import ru.astafev.crossover.architect.model.Journal;
import ru.astafev.crossover.architect.model.Number;
import ru.astafev.crossover.architect.model.Subscription;
import ru.astafev.crossover.architect.model.User;
import ru.astafev.crossover.architect.repositories.UsersRepository;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

public class EmailNotificationServiceTest {

    private EmailNotificationService service;
    private MailSender mailSender;

    @Before
    public void setUp() {
        service = new EmailNotificationService();
        mailSender = mock(MailSender.class);
        service.mailSender = mailSender;
        service.mailNotificationFrom = "no-reply@portal.com";
    }

    @Test
    public void testSendNotification() {
        final User user2 = new User();
        user2.setEmail("user@mail.org");

        final Number number = new Number();
        number.setName("number name");
        number.setJournal(new Journal());
        number.getJournal().setName("Journal name");

        service = spy(service);
        doReturn(Arrays.asList(new User(), user2)).when(service).retrieveSubscribers(number);

        service.sendNotification(number);

        verify(mailSender, times(1)).send(any(SimpleMailMessage.class));
        verify(mailSender).send(argThat(new BaseMatcher<SimpleMailMessage>() {
            @Override
            public boolean matches(Object o) {
                SimpleMailMessage message = (SimpleMailMessage) o;
                assertEquals(service.mailNotificationFrom, message.getFrom());
                assertArrayEquals(new String[]{user2.getEmail()}, message.getTo());
                return true;
            }

            @Override
            public void describeTo(Description description) {
                //do nothing
            }
        }));
    }


    @Test
    public void testRetrieveSubscribers() {
        final Number number = new Number();
        number.setJournal(new Journal());
        final Subscription[] subscriptions = {new Subscription(), new Subscription()};
        subscriptions[0].setUser("user1");
        subscriptions[1].setUser("user2");
        number.getJournal().setSubscriptions(Arrays.asList(subscriptions));

        final UsersRepository usersRepository = mock(UsersRepository.class);
        service.usersRepository = usersRepository;
        final User[] users = {new User(), new User()};
        when(usersRepository.findOne("user1")).thenReturn(users[0]);
        when(usersRepository.findOne("user2")).thenReturn(users[1]);

        assertEquals(Arrays.asList(users), service.retrieveSubscribers(number));
    }
}