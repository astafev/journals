package ru.astafev.crossover.architect.service;

import org.junit.Before;
import org.junit.Test;
import ru.astafev.crossover.architect.model.Journal;
import ru.astafev.crossover.architect.model.Number;
import ru.astafev.crossover.architect.model.Subscription;
import ru.astafev.crossover.architect.model.User;
import ru.astafev.crossover.architect.repositories.UsersRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class NotificationServiceTest {
    private NotificationService notificationService;
    private NotificationService.NewNumberNotification sender;

    @Before
    public void setUp() {
        notificationService = new NotificationService();
        sender = mock(NotificationService.NewNumberNotification.class);
        notificationService.notificationSenders = Collections.singletonList(sender);
    }

    @Test
    public void testNotify() {
        notificationService = spy(notificationService);
        final Number number = new Number();

        notificationService.notify(number);

        verify(sender).sendNotification(number);
    }

    @Test
    public void testNotifyExceptionFromOneSenderIsLoggedOnly() {
        notificationService = spy(notificationService);
        final NotificationService.NewNumberNotification sender2 = mock(NotificationService.NewNumberNotification.class);
        notificationService.notificationSenders = Arrays.asList(sender2, sender);

        final Number number = new Number();

        doThrow(new IllegalStateException()).when(sender).sendNotification(number);

        // run
        notificationService.notify(number);

        verify(sender).sendNotification(number);
        verify(sender2).sendNotification(number);
    }
}