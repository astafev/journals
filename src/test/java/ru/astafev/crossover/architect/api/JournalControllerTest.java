package ru.astafev.crossover.architect.api;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.astafev.crossover.architect.model.Journal;
import ru.astafev.crossover.architect.model.Subscription;
import ru.astafev.crossover.architect.model.Theme;
import ru.astafev.crossover.architect.repositories.JournalsRepository;
import ru.astafev.crossover.architect.repositories.SubscriptionRepository;
import ru.astafev.crossover.architect.repositories.ThemesRepository;

import java.net.URISyntaxException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class JournalControllerTest {
    private JournalController journalController;

    private JournalsRepository journalsRepository;
    private SubscriptionRepository subscriptionRepository;
    private ThemesRepository themesRepository;

    @Before
    public void setUp() {
        journalsRepository = mock(JournalsRepository.class);
        subscriptionRepository = mock(SubscriptionRepository.class);
        themesRepository = mock(ThemesRepository.class);

        journalController = new JournalController();
        journalController.journalsRepository = journalsRepository;
        journalController.subscriptionRepository = subscriptionRepository;
        journalController.themesRepository = themesRepository;
    }

    @Test
    public void testCreateNewJournal() throws Exception {
        final Journal journal = new Journal();
        final Theme newTheme = new Theme(), oldTheme = new Theme();
        oldTheme.setName("oldName");
        newTheme.setName("newName");
        when(themesRepository.findByName(oldTheme.getName())).thenReturn(Optional.of(oldTheme));
        when(themesRepository.findByName(newTheme.getName())).thenReturn(Optional.empty());
        journal.setThemes(new HashSet<>(Arrays.asList(newTheme, oldTheme)));
        when(journalsRepository.save(journal)).thenReturn(journal);

        journalController.saveJournal(journal);

        verify(themesRepository).save(newTheme);
        verify(themesRepository, times(1)).save(any(Theme.class));
        verify(journalsRepository).save(journal);
        verifyNoMoreInteractions(journalsRepository);
    }

    @Test
    public void testGetJournal() {
        final Long id = 10L;
        final Journal journal = new Journal();
        when(journalsRepository.findOne(id)).thenReturn(journal);

        ResponseEntity responseEntity = journalController.getJournal(id);

        assertEquals(journal, responseEntity.getBody());
    }

    @Test
    public void testSubscribe() throws URISyntaxException {
        final Long id = 10L;

        final Journal journal = new Journal();
        when(journalsRepository.findOne(id)).thenReturn(journal);
        final String username = mockGetUsername();
        when(subscriptionRepository.findByUserAndJournal(username, journal)).thenReturn(Optional.empty());


        ResponseEntity responseEntity = journalController.subscribe(id);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        verify(subscriptionRepository).save(argThat(new BaseMatcher<Subscription>() {
            @Override
            public boolean matches(Object o) {
                Subscription s = (Subscription) o;
                assertEquals(journal, s.getJournal());
                assertEquals(username, s.getUser());
                return true;
            }

            @Override
            public void describeTo(Description description) {
                //do nothing
            }
        }));
    }
    @Test
    public void testUnsubscribe() throws URISyntaxException {
        final Long id = 10L;

        final Journal journal = new Journal();
        when(journalsRepository.findOne(id)).thenReturn(journal);
        final String username = mockGetUsername();
        Subscription subscription = new Subscription();
        when(subscriptionRepository.findByUserAndJournal(username, journal)).thenReturn(Optional.of(subscription));


        journalController.unsubscribe(id);

        verify(subscriptionRepository).delete(subscription);
    }

    private String mockGetUsername() {
        journalController = spy(journalController);
        final String username = "username";
        doReturn(username).when(journalController).getUsername();
        return username;
    }

    @Test
    public void testGetJournals() {
        List<Journal> journals = Arrays.asList(new Journal(), new Journal());
        when(journalsRepository.findAll()).thenReturn(journals);

        ResponseEntity entity = journalController.getJournals();

        assertEquals(journals, entity.getBody());
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test
    public void testGetSubscribedJournals() {
        final String username = mockGetUsername();
        final Subscription s = new Subscription();
        final Journal journal = new Journal();
        s.setJournal(journal);
        when(subscriptionRepository.findAllByUser(username)).thenReturn(Collections.singletonList(s));

        ResponseEntity responseEntity = journalController.getSubscribedJournals();

        assertEquals(Collections.singletonList(journal), responseEntity.getBody());
    }

}