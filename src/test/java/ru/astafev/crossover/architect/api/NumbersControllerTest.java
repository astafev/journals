package ru.astafev.crossover.architect.api;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;
import ru.astafev.crossover.architect.model.Journal;
import ru.astafev.crossover.architect.model.Number;
import ru.astafev.crossover.architect.repositories.JournalsRepository;
import ru.astafev.crossover.architect.repositories.NumbersRepository;
import ru.astafev.crossover.architect.service.NotificationService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class NumbersControllerTest {
    private NumbersController numbersController;
    private NumbersRepository numberRepository;
    private JournalsRepository journalsRepository;
    private NotificationService notificationService;

    @Before
    public void setUp() {
        numberRepository = mock(NumbersRepository.class);
        journalsRepository = mock(JournalsRepository.class);
        notificationService = mock(NotificationService.class);

        numbersController = new NumbersController();
        numbersController.numberRepository = numberRepository;
        numbersController.journalsRepository = journalsRepository;
        numbersController.notificationService = notificationService;
    }

    @Test
    public void testSaveNumber() throws URISyntaxException {
        numbersController = spy(numbersController);

        final Long journalId = 10L;
        final StandardMultipartHttpServletRequest request = mock(StandardMultipartHttpServletRequest.class);
        final byte[] file = new byte[10];
        doReturn(file).when(numbersController).getFileContent(request);
        final Journal journal = new Journal();
        when(journalsRepository.findOne(journalId)).thenReturn(journal);
        final String name = "name", description = "description";
        when(request.getParameter(name)).thenReturn(name);
        when(request.getParameter(description)).thenReturn(description);
        when(numberRepository.save(any(Number.class)))
                .then((Answer<Number>) invocation -> (Number) invocation.getArguments()[0]);


        numbersController.saveNumber(journalId, request);


        verify(numberRepository).save(argThat(new BaseMatcher<Number>() {
            @Override
            public boolean matches(Object o) {
                Number n = (Number) o;
                assertEquals(journal, n.getJournal());
                assertEquals(name, n.getName());
                assertEquals(description, n.getDescription());
                assertEquals(file.length, (int) n.getFileSize());
                assertSame(file, n.getFile());
                verify(notificationService).notifyAboutNewNumber(n);
                return true;
            }

            @Override
            public void describeTo(Description description) {
                //do nothing
            }
        }));

    }

    @Test
    public void testGetFileContent() {
        final StandardMultipartHttpServletRequest request = mock(StandardMultipartHttpServletRequest.class);
        byte[] bytes = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        when(request.getParameter("file")).thenReturn("data:application/pdf;base64," +
                new String(Base64.encode(bytes), Charset.defaultCharset()));

        assertArrayEquals(bytes, numbersController.getFileContent(request));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetFileContentException() {
        final StandardMultipartHttpServletRequest request = mock(StandardMultipartHttpServletRequest.class);
        when(request.getParameter("file")).thenReturn("abcagfadsrew");
        numbersController.getFileContent(request);
    }

    @Test
    public void testDelete() {
        numbersController.delete(10L, 5L);
        verify(numberRepository).deleteByJournalIdAndId(10L, 5L);
        verifyNoMoreInteractions(numberRepository);
    }

    @Test
    public void testGetNumbers() {
        List<Number> numbers = Arrays.asList(new Number(), new Number());
        when(numberRepository.findAllByJournalIdOrderByPublishedDesc(10L)).thenReturn(numbers);

        ResponseEntity response = numbersController.getNumbers(10L);

        assertEquals(numbers, response.getBody());
    }

    @Test
    public void testGetNumber() {
        final Number number = new Number();
        when(numberRepository.findOneByJournalIdAndId(10L, 5L)).thenReturn(number);

        ResponseEntity response = numbersController.getNumber(10L, 5L);

        assertEquals(number, response.getBody());
    }

    @Test
    public void testGetFile() throws IOException {
        final byte[] file = {1, 2, 3, 4, 5, 6, 7};
        when(numberRepository.getFile(10L, 5L)).thenReturn(file);

        final HttpServletResponse response = mock(HttpServletResponse.class);
        final ServletOutputStream outputStream = mock(ServletOutputStream.class);
        when(response.getOutputStream()).thenReturn(outputStream);

        // run
        numbersController.getFile(10L, 5L, response);

        verify(outputStream).write(file);
        verify(response).flushBuffer();
    }


}