module.exports = function(config) {
  config.set({
    browsers: ['Chrome'],
    frameworks: ['jasmine'],
    files: [
      'node_modules/jquery/dist/jquery.js',
      'node_modules/angular/angular.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'node_modules/angular-resource/angular-resource.js',
      'node_modules/angular-route/angular-route.js',
      'node_modules/bootstrap/dist/js/bootstrap.js',
      '../../main/resources/static/app/app.js',
      '../../main/resources/static/app/**/*.js',
      'app/**/*.spec.js'
    ]
  });
};