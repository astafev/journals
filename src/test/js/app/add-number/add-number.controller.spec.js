describe('AddNumberController ', function () {
  'use strict';
  
  beforeEach(module('journalsPortal'));
  
  var $controller, Numbers, Errors;

  beforeEach(inject(function (_$controller_, _Numbers_, _Errors_) {
    $controller = _$controller_;
    Numbers = _Numbers_;
    Errors = _Errors_;
  }));

  describe('AddNumberController', function () {
    it('defines method method save in scope', function () {
      var $scope = {};
      $controller('AddNumberController', {$scope: $scope});

      expect($scope.save).toEqual(jasmine.any(Function));
    });

    it('method save runs save method of Numbers service', function () {
      var $scope = {};
      spyOn(Numbers, 'save');

      $controller('AddNumberController', {$scope: $scope, Numbers: Numbers});
      $scope.save();

      expect(Numbers.save).toHaveBeenCalled();
    });

    it('save on success sets message', function () {
      var $scope = {};
      spyOn(Numbers, 'save');

      $controller('AddNumberController', {$scope: $scope, Numbers: Numbers});
      $scope.save();

      var successCallback = Numbers.save.calls.argsFor(0)[2];
      expect(successCallback).toEqual(jasmine.any(Function));
      successCallback(null, function(){});
      expect($scope.message).toEqual('The number was successfully saved, go back to ');
      expect($scope.resultPage).toEqual("journal page");
    });

    it('save on error sets error message', function () {
      var $scope = {};
      spyOn(Numbers, 'save');

      $controller('AddNumberController', 
        {$scope: $scope, Numbers: Numbers, Errors: Errors});
      $scope.save();
      $scope.resultLink = "sdf";
      $scope.resultPage = "sadf";

      var failCallback = Numbers.save.calls.argsFor(0)[3];
      expect(failCallback).toEqual(jasmine.any(Function));
      var errorMessage = 'error message';
      spyOn(Errors, 'parseError').and.returnValue(errorMessage);
      failCallback({});
      expect($scope.message).toEqual(errorMessage);
      expect($scope.resultLink).toBeUndefined();
      expect($scope.resultPage).toBeUndefined();
    });
    
  });
});