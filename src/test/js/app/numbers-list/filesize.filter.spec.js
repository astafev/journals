describe('filesize filter', function () {
  'use strict';

  var $filter;

  beforeEach(function () {
    module('journalsPortal');

    inject(function (_$filter_) {
      $filter = _$filter_;
    });
  });

  it('bytes', function () {
    var foo = [
      {size: 200, string: '200 Bytes'},
      {size: 300, string: '300 Bytes'}
    ];
    foo.forEach(function(val){
      expect($filter('filesize')(val.size)).toEqual(val.string);
    });
  });

  it('kilobytes', function () {
    var foo = [
      {size: 1024, string: '1.00 Kb'},
      {size: 2048, string: '2.00 Kb'},
      {size: 2000, string: '1.95 Kb'},
      {size: 500*1024, string: '500.00 Kb'}
    ];
    foo.forEach(function(val){
      expect($filter('filesize')(val.size)).toEqual(val.string);
    });
  });

  it('megabytes', function () {
    var foo = [
      {size: 1024*1024, string: '1.00 Mb'},
      {size: 2048*1024, string: '2.00 Mb'},
      {size: 2000*1024, string: '1.95 Mb'},
      {size: 500*1024*1024, string: '500.00 Mb'}
    ];
    foo.forEach(function(val){
      expect($filter('filesize')(val.size)).toEqual(val.string);
    });
  });

  it('gigabytes', function () {
    var foo = [
      {size: 1024*1024*1024, string: '1.00 Gb'},
      {size: 2048*1024*1024, string: '2.00 Gb'},
      {size: 2000*1024*1024, string: '1.95 Gb'},
      {size: 500*1024*1024*1024, string: '500.00 Gb'}
    ];
    foo.forEach(function(val){
      expect($filter('filesize')(val.size)).toEqual(val.string);
    });
  });
  it('Terrabytes', function () {
    var foo = [
      {size: 1024*1024*1024*1024, string: '1.00 Tb'}
    ];
    foo.forEach(function(val){
      expect($filter('filesize')(val.size)).toEqual(val.string);
    });
  });
});