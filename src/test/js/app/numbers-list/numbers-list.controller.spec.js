describe('NumbersListController ', function () {
  'use strict';

  var $controller, Numbers, Journals, $scope;

  beforeEach(module('journalsPortal'));
  beforeEach(function () {
    $scope = {}
  });
  beforeEach(inject(function (_$controller_, _Numbers_, _Journals_) {
    $controller = _$controller_;
    Numbers = _Numbers_;
    Journals = _Journals_;
  }));


  describe('$scope.subscribedJournals ', function () {
    it('is being set on init', function () {
      var journals = [{name: 'Journal 1'}];
      spyOn(Journals, 'getSubscribed').and.returnValue(journals);

      $controller('NumbersListController', {$scope: $scope, Journals: Journals});

      expect($scope.subscribedJournals).toEqual(journals);
    });

    it('on success tries to find active journal', function () {
      var journals = [{name: 'Journal 1', id: 1}];
      spyOn(Journals, 'getSubscribed').and.returnValue(journals);

      $controller('NumbersListController', {
        $scope: $scope,
        Journals: Journals,
        $routeParams: {journalId: 1}
      });
      var successCallback = Journals.getSubscribed.calls.argsFor(0)[1];
      successCallback(journals);

      expect($scope.subscribedJournals).toEqual(journals);
      expect($scope.activeJournal).not.toBeNull();
      expect($scope.activeJournal.subscribed).toEqual(true);
    });

    it('on success additional request to get journal info is made if current journal is not in subscribed list', function () {
      var journals = [{name: 'Journal 1', id: 1}];
      spyOn(Journals, 'getSubscribed').and.returnValue(journals);

      $controller('NumbersListController', {
        $scope: $scope,
        Journals: Journals,
        $routeParams: {journalId: 2}
      });
      var activeJournal = {name: 'Journal 2', id: 2};
      spyOn(Journals, 'getOne').and.returnValue(activeJournal);
      var successCallback = Journals.getSubscribed.calls.argsFor(0)[1];
      successCallback(journals);

      expect($scope.subscribedJournals).toEqual(journals);
      expect($scope.activeJournal).toEqual(activeJournal);
      expect($scope.activeJournal.subscribed).not.toEqual(true);
    });
  });

  describe('$scope.numbers', function () {
    it("is being set on controller init", function () {
      var numbers = [{name: 'Number 1', id: 1}];
      spyOn(Numbers, 'get').and.returnValue(numbers);
      $controller('NumbersListController', {
        $scope: $scope,
        Numbers: Numbers
      });
      expect($scope.numbers).toEqual(numbers);
    })
  });

  describe('method subscribe', function () {
    it("is defined and run Journals.subscribe", function () {
      spyOn(Journals, 'subscribe');

      $controller('NumbersListController', {
        $scope: $scope,
        Journals: Journals
      });
      expect($scope.subscribe).toEqual(jasmine.any(Function));

      $scope.subscribe();
      expect(Journals.subscribe).toHaveBeenCalled();
    });

    it("on success updates model", function () {
      

      spyOn(Journals, 'subscribe');

      $controller('NumbersListController', {
        $scope: $scope,
        Journals: Journals
      });
      expect($scope.subscribe).toEqual(jasmine.any(Function));
      $scope.subscribe();

      var successCallback = Journals.subscribe.calls.argsFor(0)[1];
      $scope.activeJournal = {};
      successCallback();

      expect($scope.activeJournal.subscribed).toEqual(true)
    })
  });
  //--------------------------------------------------
  // Unsubscribe
  //----------------------------------------
  describe('method unsubscrbe', function () {
    it("is defined and run Journals.unsubscribe", function () {
      spyOn(Journals, 'unsubscribe');

      $controller('NumbersListController', {
        $scope: $scope,
        Journals: Journals
      });
      expect($scope.unsubscribe).toEqual(jasmine.any(Function));

      $scope.unsubscribe();
      expect(Journals.unsubscribe).toHaveBeenCalled();
    });

    it("on success updates model", function () {
      spyOn(Journals, 'unsubscribe');

      $controller('NumbersListController', {
        $scope: $scope,
        Journals: Journals
      });
      expect($scope.unsubscribe).toEqual(jasmine.any(Function));
      $scope.unsubscribe();

      var successCallback = Journals.unsubscribe.calls.argsFor(0)[1];
      $scope.activeJournal = {};
      successCallback();

      expect($scope.activeJournal.subscribed).toEqual(false)
    })
  });


});