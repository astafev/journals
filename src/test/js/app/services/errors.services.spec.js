describe('Errors', function () {
  'use strict';

  var Errors;

  beforeEach(module('journalsPortal'));

  beforeEach(function () {
    inject(function ($injector) {
      Errors = $injector.get('Errors');
    });
  });

  describe('function parseError', function () {
      it('should return error message if it present', function () {
        expect(Errors.parseError({error: 'message'})).toEqual('message');
      });
      it('should parse spring validation message', function () {
        var e = {
          data: {
            errors: [{
              field: "name",
              defaultMessage: "can't be null"
            }]
          }
        };
        expect(Errors.parseError(e)).toEqual('error occured: name can\'t be null; ');
      });
    }
  );
});