describe('AddJournalController ', function () {
  beforeEach(module('journalsPortal'));

  var $controller, Journals, Errors;

  beforeEach(inject(function (_$controller_, _Journals_, _Errors_) {
    $controller = _$controller_;
    Journals = _Journals_;
    Errors = _Errors_;
  }));

  it('defines method method addTheme in scope', function () {
    var $scope = {};
    $controller('AddJournalController', {$scope: $scope});

    expect($scope.addTheme).toEqual(jasmine.any(Function));
  });

  it('method addTheme adds one element to themes in journal', function () {
    var $scope = {};
    $controller('AddJournalController', {$scope: $scope});

    expect($scope.journal).not.toBeNull();
    expect($scope.journal.themes.length).toEqual(0);

    $scope.addTheme();

    expect($scope.journal.themes.length).toEqual(1);
  });

  it('defines method method deleteTheme in scope', function () {
    var $scope = {};
    $controller('AddJournalController', {$scope: $scope});

    expect($scope.addTheme).toEqual(jasmine.any(Function));
  });

  it('method deleteTheme removes one element from themes in journal', function () {
    var $scope = {};
    $controller('AddJournalController', {$scope: $scope});

    expect($scope.journal).not.toBeNull();
    expect($scope.journal.themes.length).toEqual(0);

    $scope.addTheme();

    expect($scope.journal.themes.length).toEqual(1);
    $scope.addTheme();
    $scope.journal.themes[1].name = 'name';

    $scope.deleteTheme(0);
    expect($scope.journal.themes.length).toEqual(1);
    expect($scope.journal.themes[0].name).toEqual('name');
  });

  describe('save:', function () {

    it('defines method method save in scope', function () {
      var $scope = {};
      $controller('AddJournalController', {$scope: $scope});

      expect($scope.save).toEqual(jasmine.any(Function));
    });

    it('method save runs save method of Journals service', function () {
      var $scope = {};
      spyOn(Journals, 'save');

      $controller('AddJournalController', {$scope: $scope, Journals: Journals});
      $scope.save();

      expect(Journals.save).toHaveBeenCalled();
    });

    it('save on success sets message', function () {
      var $scope = {};
      spyOn(Journals, 'save');

      $controller('AddJournalController', {$scope: $scope, Journals: Journals});
      $scope.save();

      var successCallback = Journals.save.calls.argsFor(0)[2];
      expect(successCallback).toEqual(jasmine.any(Function));
      var headers = function () {
        return {location: '/api/journals/123'}
      };
      successCallback(null, headers);
      expect($scope.message).toEqual('The journal was successfully saved, visit ');
      expect($scope.resultPage).toEqual("journal page");
      expect($scope.resultLink).toEqual("#/journals/123");
    });

    it('save on error sets error message', function () {
      var $scope = {};
      spyOn(Journals, 'save');

      $controller('AddJournalController',
        {$scope: $scope, Journals: Journals, Errors: Errors});
      $scope.save();
      $scope.resultLink = "sdf";
      $scope.resultPage = "sadf";
      
      var failCallback = Journals.save.calls.argsFor(0)[3];
      expect(failCallback).toEqual(jasmine.any(Function));

      var errorMessage = 'error message';
      spyOn(Errors, 'parseError').and.returnValue(errorMessage);
      failCallback({});
      expect($scope.message).toEqual(errorMessage);
      expect($scope.resultLink).toBeUndefined();
      expect($scope.resultPage).toBeUndefined();
    });
  });
});