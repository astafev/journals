describe('JournalsListController ', function () {
  'use strict';
  

  beforeEach(module('journalsPortal'));

  var $controller, Journals;

  beforeEach(inject(function (_$controller_, _Journals_) {
    $controller = _$controller_;
    Journals = _Journals_;
  }));

  it('calls get method of Journals', function () {
    var $scope = {};
    var journals = [{name:'JournalName'}];
    spyOn(Journals, 'get').and.returnValue(journals);
    
    $controller('JournalsListController', {$scope: $scope, Journals:Journals});

    expect($scope.journals).toEqual(journals);
  });
});