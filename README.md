Prerequisites
-------------
 - Gradle >= 2.4
 - java 1.8
 - Node JS and npm for running frontend unit tests (node v4.4.4, npm v2.15.1)


To Run
------
`gradle bootRun`

It will run a web server on port 8080.
Local DB will be created from file `data.sql` automatically.

If port 8080 is taken by another application, change property `server.port` in `src/main/resources/application.yml`.

Database will be created with 2 users (login/passowrd): admin/admin and user/user.
Currently there's no registration page. So no way to create another user.

To test email notifications you need to make the following changes in `application.yml` (under src/main/resources):
```
notification.email = true
spring.mail:
    host: <smtp host, for example smtp.gmail.com>
    username: <username, for example astafev.evgeny@gmail.com>
    password: <password (your email account password)>
    protocol: smtps
    port: <smtp port here, for example, 465>
```
and in data.sql (under src/main/resources too): in last statement update email value to get something like this:

`UPDATE users SET email = 'astafev.evgeny@gmail.com' WHERE username = 'user';`

Email is needed to be updated in data.sql because there's no registration page implemented (because of lack of time).



##### Tests
`gradle test` to run tests for java part. Currently coverage is about 74% LoC
(getters/setters in models, configuration and simple methods in class ViewController left uncovered).

For frontend:
1. Go to src/test/js directory
2. run `npm install` it will install karma and dependencies in local directory
3. run tests with `npm test`

-----------------------------------------------------------------------------------

About the application
--------------------

##### Overall objective (from Crossover)

Create the architecture and design of a medical journals publication and subscription system. Implement the system's
 services and applications.

##### Journals and numbers

Journals have multiple *numbers*. Journals can have description and themes.
I wanted to make search by themes to address the requirement
"A web portal to find and subscribe to journals of their interest".
 But looks like won't finish it.
On frontend search works by all fields (description and themes).

Number is a pdf file with its own description.

##### Roles model

There are 2 roles: PUBLISHER and PUBLIC_USER.
 Public user can subscribe/unsubscribe to journals and download numbers.
 When a new number is published in one of subscribed journals, user gets notification about it in email.

Publishers additionally can create journals and numbers.

Probably I won't be able to link journals and users that published them and restrict access accordingly.
Now all publishers can publish a number to any journal.

Technology stack
--------------
Backend:
 - Spring Boot
 - Spring MVC
 - Spring Data
 - Spring Security
 - Thymeleaf
 - Hibernate
 - Lombok
 - HSQLDB

Frontend:
 - Angular
 - Bootstrap

Application can be packaged as executable jar or can be launched directly from Gradle.
Frontend part is written on Angular. Database is HSQLDB, but can be changed simply.
HSQLDB was chosen because it can be simply packaged in the distribution.

Frontend on Angular calls REST services written in java, Spring MVC.

Project structure:
```
/src/
    main/ - application code
        /java/ru/astafev/crossover/architect – java sources
            Application.java – Spring boot start point, class with main method
            /api – REST services (on Spring MVC)
            /config – configuration files
            /model – DB entities
            /repositories – repositories that interacts with DB (on Spring Data)
            /services - spring services (other functions like notification email sender)
            /view – controller that serve views
        /resources
            /static – part of frontend application that do not require any modifications on backend (styles, scripts, views)
            /template – Thymeleaf templates
            application.yml – configuration file
            data.sql – data to fill DB on application start
            logback.xml – logger configuration
    test/ - unit test code
        java/ - java unit tests
        js/ - js unit tests (in fact NodeJS application)
```

TODO
======
 - validation on both frontend and backend
 - create service with user settings and restrict access on frontend where necessary (exclude thymeleaf)
 - new user registration page and pretty login page
 - restrict access of publishers to only their own journals
 - delete functionality for publishers